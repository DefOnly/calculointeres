<%-- 
    Document   : index
    Created on : 29-03-2020, 16:44:48
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>CALCULDORA DE INTERÉS</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <!-- FRAMEWORK BOOTSTRAP-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    
    </head>
    <body>
         <h1></h1>
         
         <form name="form" action="controller" method="POST">
         
         <div style="width: 20%; padding: 20px">
         <div class="form-group">
              Ingrese Capital: <input type="text" class="form-control" placeholder="Capital" name="capital"/>
         </div>
         
           <div class="form-group">
              Ingrese tasa anual: <input type="text" class="form-control" placeholder="Tasa Anual" name="tasa"/>
         </div>
             
             <div class="form-group">
              Ingrese la cantidad de años: <input type="text" class="form-control" placeholder="Años" name="year"/>
         </div>
         </div>
         
         <div style="padding: 20px; position: relative; bottom: 42px">
         <button type="submit" class="btn btn-primary">Calcular</button>
         </div>
             
         </form>
    </body>
</html>
