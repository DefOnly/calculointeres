/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.modelo;

/**
 *
 * @author admin
 */
public class CalcularInteres {
    private double capital;
    private int tasa;
    private int year;

    public CalcularInteres(double capital, int tasa, int year) {
        this.capital = capital;
        this.tasa = tasa;
        this.year = year;
    }

    public void setCapital(double capital) {
        this.capital = capital;
    }

    public void setTasa(int tasa) {
        this.tasa = tasa;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getCapital() {
        return capital;
    }

    public int getTasa() {
        return tasa;
    }

    public int getYear() {
        return year;
    }
    
    public double getCalculo(){
        double t = tasa/100;
        return capital * t * year;
    }
    
}
